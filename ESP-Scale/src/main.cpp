#include <Arduino.h>
#include <HX711.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Include the wifi_config.hpp file with wifi credentials
#include "wifi_config.hpp"

// Define the topic for the MQTT messages
const char *topic = "krpavlu/RR";

// Define the MQTT server and port
const char *mqttServer = "mqtt.eclipseprojects.io";
const int mqttPort = 1883;

// Create a wifi and MQTT client
WiFiClient wifiClient;
PubSubClient client(mqttServer, mqttPort, wifiClient);

// Define the timing of messages
unsigned long DELAY_TIME = 1000;
unsigned long delayStart = 0;

// Define the message buffer
char msg[50];

// Define the loadcell
HX711 loadcell;
long weight;

// Function to convert MAC address to string
String macToStr(const uint8_t *mac)
{
  String result;

  for (int i = 0; i < 6; ++i)
  {
    result += String(mac[i], 16);

    if (i < 5)
    {
      result += ':';
    }
  }
  return result;
}

// Function to reconnect to wifi and MQTT
void reconnect()
{
  // attempt to connect to the wifi if connection is lost
  if (WiFi.status() != WL_CONNECTED)
  {
    // debug printing
    Serial.print("Connecting to ");
    Serial.println(ssid);

    // loop while we wait for connection
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);
      Serial.print(".");
    }

    // print out some more debug once connected
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }

  // make sure we are connected to WIFI before attemping to reconnect to MQTT
  if (WiFi.status() == WL_CONNECTED)
  {
    // Loop until we're reconnected to the MQTT server
    while (!client.connected())
    {
      Serial.print("Attempting MQTT connection...");

      String clientName;
      clientName += "esp8266-";
      uint8_t mac[6];
      WiFi.macAddress(mac);
      clientName += macToStr(mac);

      if (client.connect((char *)clientName.c_str()))
      {
        Serial.print("\tMQTT Connected");
      }
    }
  }
}

void setup()
{
  // Set the pin mode for the LED and start the serial connection
  pinMode(2, OUTPUT);
  Serial.begin(9600);
  delay(10);

  // Connect to the wifi
  WiFi.begin(ssid, password);

  // Connect to the MQTT server
  reconnect();

  // Start the loadcell
  Serial.println("Loadcell setup");
  loadcell.begin(4, 0);
  loadcell.set_scale();
  loadcell.tare();

  // Start the delay
  delayStart = millis();
}

void loop()
{
  if ((millis() - delayStart) >= DELAY_TIME)
  {
    if (!client.connected())
    {
      reconnect();
    }
    delayStart += DELAY_TIME;
    weight = loadcell.get_units(10);
    snprintf(msg, 75, "%ld", weight);
    client.publish(topic, msg);
    Serial.println(msg);
    digitalWrite(2, LOW);
    delay(10);
    digitalWrite(2, HIGH);
  }
  client.loop();
}