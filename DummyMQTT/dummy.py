from flask import Flask
from flask_mqtt import Mqtt
import random
import time
import threading

mqtt_topics = ["krpavlu/FR", "krpavlu/FL", "krpavlu/RR", "krpavlu/RL"]

app = Flask(__name__)

app.config['MQTT_BROKER_URL'] = 'mqtt.eclipseprojects.io'
app.config['MQTT_BROKER_PORT'] = 1883
mqtt = Mqtt(app)

def publish_random_values():
    while True:
        for topic in mqtt_topics:
            random_value = random.randint(0, 100)  # Generování náhodné hodnoty
            mqtt.publish(topic, random_value)
            print(f"Published {random_value} to {topic}")
        time.sleep(1)  # Čekání 1 sekundu před odesláním dalších hodnot

@app.route('/')
def index():
    return "MQTT Publisher is running!"

if __name__ == '__main__':
    # Spuštění publikačního vlákna
    threading.Thread(target=publish_random_values).start()
    app.run(debug=True, port=5001)