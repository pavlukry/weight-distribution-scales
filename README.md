# Weight Distribution Scales

## Introduction

Weight Distribution Scales is a project developed in the course B0B37NSI. The task is to create a model of a system of scales that can determine the exact distribution of a weighed object. The main use is to determine the weight distribution of the eForce Prague Formula racing cars. 

## Assignment

The objective is to create a prototype of four weighing units based on the ESP8266 microcontroller, in particular on the [Wemos D1 Mini](https://www.laskakit.cz/wemos-d1-mini-esp8266-wifi-modul/) development board. This platform was used because of its small size, sufficient interfaces, wide documentation and low cost. 

A [GML653A 40kg strain gauge](https://www.alibaba.com/product-detail/GML653A-40kg-strain-gauge-mimi-load_60448398184.html) (load cell) will be connected to each of these modules. These strain gauges were available to me from past projects. Their advantage is their small size.  The information from them will be converted from analogue to digital value by the [HX711 ADC](https://cdn.sparkfun.com/datasheets/Sensors/ForceFlex/hx711_english.pdf). These converters were used because of their high availability and ease of use. 

Each of these units will send information about the status of the weight sensor at regular intervals using MQTT. The information from all the scales will then be managed by a flask application in which the data can then be interpreted, the scales calibrated and so on. 

## Tasks

### Required

- [x] Implement strain gauge measurement using ADC HX711 on Wemos D1 Mini development board
- [x] Connect the development board to wifi
- [x] Start sending data to the MQTT server
- [x] Send status messages about the current state of the strain gauge to the MQTT server
- [x] Create a flash application that will read data from topics created by development boards
- [x] Add a simple user interface

### Optional:
- [x] Add a simple calibration option
- [x] Add option to zero the weights

## Execution

The platformio interface was used to program the development boards. This makes it easy to manage projects on common development boards and would make it possible to switch to another board in the future. Platformio also has a number of pre-installed libraries that can be implemented in projects. 

For the time being, the freely available [eclipseproject.io](https://mqtt.eclipseprojects.io) was chosen as the MQTT server.



## Findings

The used strain gauge [GML653A](https://www.alibaba.com/product-detail/GML653A-40kg-strain-gauge-mimi-load_60448398184.html) is not suitable for real application of this type. The maximum range of one strain gauge is 40kg, which is not sufficient for a real car weighing around 220kg (without pilot). Also the car may not have an even weight distribution, one of the axles may be loaded more than the other. Therefore, a realistic application would require strain gauges with a maximum weight in the range of 80-120kg. On the other hand, for testing purposes these weights were also not ideal. The device was tested with weights up to 2 kg. For these weights, on the other hand, the range was quite large and the measurement was inaccurate at these weights. 

After testing MQTT communication, I am not sure if its use was optimal. It is desirable for the form of aplication that the data goes out at very short intervals so that the display is almost live. However, for now the MQTT application is sufficient in a real implementation it would be worth trying other means of communication. 

## Possible future improvements for real implementation

A battery power circuit would need to be added to the HW, ideally using a lithium cell charged via one of the USB standards. 

There would also have to be an overall mechanical structure that could withstand the pressure of the required weight. 

This also entails the use of other strain gauges that would carry the required weights.

It would also be worth considering using other method of strain gauge measurements, for example using differential signals and transfer functions. This could help with the accuracy of the measurements.

As already mentioned, it was found that MQTT communication was not an ideal choice, so another solution could be explored. This could also affect the HW design with the need for a different communication protocol for example. The software would also have to be redesigned.

It would also be nice to create a fifth HW device with, for example, a display, which would have the same functionality as the web interface. For common use of mechanics this could be easier.