from flask import Flask, Response, render_template
from flask_mqtt import Mqtt
from flask_socketio import SocketIO
import time

mqtt_data = {
    'FR': {'value': None, 'timestamp': None},
    'FL': {'value': None, 'timestamp': None},
    'RR': {'value': None, 'timestamp': None},
    'RL': {'value': None, 'timestamp': None}
}

app = Flask(__name__)
app.config['MQTT_BROKER_URL'] = 'mqtt.eclipseprojects.io'
app.config['MQTT_BROKER_PORT'] = 1883
mqtt = Mqtt(app)
socketio = SocketIO(app)
 
@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    mqtt.subscribe('krpavlu/#')
 
@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    topic = message.topic.split('/')[-1]
    payload = int(message.payload.decode())
    current_time = time.time()

    if topic in mqtt_data:
        mqtt_data[topic]['value'] = payload
        mqtt_data[topic]['timestamp'] = current_time
    socketio.emit('mqtt_message', mqtt_data)


@app.route('/')
def index():
    return render_template('index.html')

if __name__ == "__main__":  
    socketio.run(app, debug=True)